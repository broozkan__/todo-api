const mongoose = require('mongoose')
var aggregatePaginate = require("mongoose-aggregate-paginate-v2")

const todoSchema = mongoose.Schema({
    id: {
        type: Number,
        default: Math.floor(Math.random() * 1000)
    },
    title: {
        type: String,
        required: true
    }
})


todoSchema.plugin(aggregatePaginate);


module.exports.todoSchema = todoSchema
module.exports.todoModel = mongoose.model('Todo', todoSchema)