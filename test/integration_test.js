const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const app = require('../index');
const should = chai.should();
const expect = chai.expect;


describe('GET /api/todos', () => {
    it('should return a list of todos when called', done => {
        chai
            .request(app)
            .get('/api/todos/42')
            .end((err, res) => {
                res.should.have.status(200);
                done()
            })

    }).timeout(20000);
});