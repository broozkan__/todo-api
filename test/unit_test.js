const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const app = require('../index');
const should = chai.should();
const expect = chai.expect;


describe('POST /api/todos', () => {
    it('should create a todo object', (done) => {
        chai
            .request(app)
            .post('/api/todos')
            .set('content-type', 'application/json')
            .send({ title: 'test' })
            .end((err, res) => {
                res.should.have.status(201);
                done();
            });
    }).timeout(10000)
});