const TodoModel = require('../Models/ModelTodo')

class Todo {
    constructor(title) {
        this.title = title
    }


    async save(cb) {
        const todo = new TodoModel.todoModel(this)

        await todo.save((err, res) => {
            if (err) {
                cb({
                    response: false,
                    responseData: err.message,
                    status: 400
                })
            } else {

                cb({
                    response: true,
                    responseData: res,
                    status: 201
                })
            }
        })
    }

}

module.exports = Todo
