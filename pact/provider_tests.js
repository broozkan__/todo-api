const { Verifier } = require('@pact-foundation/pact');
const packageJson = require('../package.json')


return new Verifier().verifyProvider({
  provider: 'todo-provider',
  providerBaseUrl: 'http://localhost:8080',

  // Fetch pacts from broker
  pactBrokerUrl: process.env.PACT_BROKER_URL,
  pactBrokerToken: process.env.PACT_BROKER_TOKEN,

  publishVerificationResult: true,
  providerVersion: packageJson.version
});