# Todo App RestAPI Service

- A RESTAPI service for creating todo list. It provides backend service for todo app with CI/CD automation pipeline. 
- You can see a hosted version on <a href="http://35.238.82.170:80" target="_blank">here</a>.

#### Architecture & Development
- This service has a RESTful API design because of scalibility and compilability with microservice architecture. And also RESTful API is quite adorable way to expose microservice architecture. 
During development process TDD procedure adopted. 

## Requirements

###### for build, test
* A .env file should include
    - DB_CONNECTION (Atlas:MongoDB connection url)

    - PACT_BROKER_URL (A pact token for CDC test `pactflow.io`)
    
    - PACT_BROKER_TOKEN (A pact token for CDC test `pactflow.io`)

* Node 8 or above
* Mocha
* Git

###### for CI/CD
* Gitlab CI/CD environment variables
    - DOCKER_USERNAME (for authenticating to Docker)
    - DOCKER_PASSWORD (for authenticating to Docker)
    - GCLOUD_SERVICE_KEY (for deploying GKE)


## Installation

Clone the repo and install the dependencies.
```bash
git clone https://gitlab.com/broozkan__/todo-api.git
cd todo-api
```
```bash
npm install
```

## Testing
This app has unit, integration and CDC test as provider.
These tests also running on gitlab-ci pipeline with `gitlab-ci.yml` file
1: Unit test for create todo:
```bash
mocha test/unit_test.js
```
2: Integration test:
```bash
mocha test/integration_test.js
```
3: Verify CDC as provider (which also executing on CI/CD pipeline):
```bash
npm run test:pact
```

## Build
###### With Using Docker
You can also run this app as a Docker container:

- Step 1: Clone the repo
```bash
git clone https://gitlab.com/broozkan__/todo-api.git
cd todo-api
```
- Step 2: Build the Docker image

```bash
docker build -t todo-api .
```

- Step 3: Run the Docker container locally:

```bash
docker run -p 8080:8080 -d todo-api
```

###### With Npm

You can run it on `http://localhost` with:
```bash
npm run start
```

### Deployment

###### Deploy on GKE
- For deploying your application to GKE you have to have a service account which should have the right permissions for deployment.
- You can read <a href="https://cloud.google.com/kubernetes-engine/docs/how-to/iam" target="_blank">this</a> this guide for how to do that.

After having the right permissions:
- Step 1: Download the service account key file from Google Cloud

- Step 2: Active service account:
```bash
gcloud auth activate-service-account --key-file /path/to/gcloud-service-key.json
```

- Step 3: Set Google Cloud project id and zone informations:
```bash
gcloud config set project $GKE_PROJECT_ID
gcloud config set compute/zone $GKE_ZONE
```

- Step 4: Get cluster credentials (create a cluster on GKE if it is not exist) :
```bash
gcloud container clusters get-credentials $GKE_CLUSTER_NAME
```

- Step 5: Apply cloud build informations:
```bash
kubectl apply -f simple-app.yaml
```

- Step 6: Restart service for update image (optional):
```bash
kubectl rollout restart deployment/simple-node
```
