const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const TodoModel = require('../Models/ModelTodo')
const Todo = require('../Classes/ClassTodo')


// get todo list
router.get('/:todoId?', async (req, res) => {

    let pipeline = []
    let limit = 50


    if (req.params.todoId) {


        pipeline = [{
            $match: {
                id: parseInt(req.params.todoId)
            }
        }]
    }


    const aggregate = TodoModel.todoModel.aggregate(pipeline)


    const options = {
        page: req.params.page,
        limit: limit
    }

    TodoModel.todoModel.aggregatePaginate(aggregate, options, (err, result) => {
        res.send(result.docs)
    })
})




router.post('/', async (req, res) => {


    const todo = new Todo(
        req.body.title
    )


    todo.save((result) => {

        res.status(result.status)
        res.json(result.responseData)
    })

})



module.exports = router;
